import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

const Square = props => (
  <button className="square" onClick={props.handleSquareClick}>
    {props.value}
  </button>
);

class Board extends React.Component {
  renderSquare(i) {
    return (
      <Square
        handleSquareClick={() => this.props.handleSquareClick(i)}
        value={this.props.squares[i]}
      />
    );
  }

  render() {
    return (
      <div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{ squares: Array(9).fill(null) }],
      stepNumber: 0,
      xIsNext: true
    };
  }
  handleSquareClick = i => {
    this.setState(prevState => {
      let newState = { ...prevState };
      newState.history = newState.history.slice(0, prevState.stepNumber + 1);
      const newStateLastHistoryIndex = [
        ...newState.history[newState.history.length - 1].squares
      ];
      if (
        !this.calculateWinner(newStateLastHistoryIndex) &&
        !newStateLastHistoryIndex[i]
      ) {
        newStateLastHistoryIndex[i] = prevState.xIsNext ? "X" : "O";
        newState.xIsNext = !prevState.xIsNext;
        newState.history = [
          ...newState.history,
          { squares: newStateLastHistoryIndex }
        ];
        newState.stepNumber = newState.history.length - 1;
        return newState;
      } else {
        return prevState;
      }
    });
  };
  calculateWinner = squares => {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (
        squares[a] &&
        squares[a] === squares[b] &&
        squares[a] === squares[c]
      ) {
        return squares[a];
      }
    }
    return null;
  };
  jumpTo = step => {
    this.setState(() => ({ stepNumber: step, xIsNext: step % 2 === 0 }));
  };
  render() {
    const { history, xIsNext } = this.state;
    const current = history[this.state.stepNumber];
    const winner = this.calculateWinner(current.squares);
    const moves = history.map((step, move) => {
      const desc = move ? `go to #${move}` : `go to game start`;
      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      );
    });
    let status;
    if (winner) {
      status = `Winner : ${winner}`;
    } else {
      status = `Next : ${xIsNext ? "X" : "O"}`;
    }
    return (
      <div className="game">
        <div className="game-board">
          <Board
            status={status}
            squares={current.squares}
            handleSquareClick={i => this.handleSquareClick(i)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

// ========================================

ReactDOM.render(<Game />, document.getElementById("root"));
